# -*- coding: utf-8 -*-

from odoo import models, fields, api

class type_book(models.Model):
    _name = 'manual.type_book'

    name = fields.Char(required=True )
    description = fields.Text()

class class_number(models.Model):
    _name = 'manual.class_number'

    name = fields.Char(required=True)
    description = fields.Text()

class direction_book(models.Model):
    _name = 'manual.direction_book'

    name = fields.Char(required=True)
    description = fields.Text()

class science_degree(models.Model):
    _name = 'manual.science_degree'

    name = fields.Char(required=True)
    coefficient=fields.Float()
    description=fields.Char()

class types_expertise(models.Model):
    _name = 'manual.types_expertise'

    name = fields.Char(required=True)
    description=fields.Char()

class direction_book(models.Model):
    _name = 'manual.direction_book'

    name = fields.Char(required=True)
    coefficient=fields.Float()
    description=fields.Char()

class types_organ(models.Model):
    _name = 'manual.type_organization'

    name = fields.Char(required=True)
    description=fields.Char()

class publishing_house(models.Model):
    _name = 'manual.publishing_house'

    name = fields.Char(required=True)
    adress=fields.Char()
    email=fields.Char()
    phone=fields.Char()

class lang_book(models.Model):
    _name = 'manual.lang_book'

    name = fields.Char(required=True)
    description=fields.Char()

class author(models.Model):
    _name = 'manual.author'

    name = fields.Char()
    lastName=fields.Char()
    patronymic=fields.Char()
    date_birth=fields.Date()
    country=fields.Char()
    attachment_ids=fields.Many2many("manual.attachment_authors",ondelete='restrict', string="Attachment", required=False)

class book(models.Model):
    _name = 'manual.book'

    name = fields.Char()
    subject=fields.Char()
    type_id=fields.Many2one('manual.type_book', ondelete='restrict', string="Type Book", required=True)
    lang_id=fields.Many2one('manual.lang_book',ondelete='restrict', string='Language Book', required=True)
    lang_read_id=fields.Many2one('manual.lang_book',ondelete='restrict', string='Learing language book', required=True)
    class_id = fields.Many2one('manual.class_number', ondelete='restrict', string='Class', required=True)
    year=fields.Date()
    publishing_house_id=fields.Many2one('manual.publishing_house', ondelete='restrict', string='Publishing house', required=True)
    direction_id = fields.Many2one('manual.direction_book', ondelete='restrict', string='Direction',required=True)
    author_ids = fields.Many2many('manual.author', ondelete='restrict', string='AuthorID',required=False)
    department_id = fields.Many2one('hr.department',compute='_compute_department')
    


    @api.depends("create_uid")
    def _compute_department(self):
        for mail in self:
            Employee = mail.env['hr.employee'].search([['user_id','=','create_uid']],limit=1)
            if(len(Employee)>0):
                book.department_id=Employee.department_id

class experts(models.Model):
    _name = 'manual.experts'

    name = fields.Char()
    lastName=fields.Char()
    date_birth=fields.Datetime()
    IIN=fields.Integer(string="IIN")
    diplom=fields.Char()
    science_degree_id=fields.Many2one('manual.science_degree', ondelete='restrict', string="Science degree", required=True)
    subject=fields.Char()
    place_work=fields.Char()
    attachment_ids=fields.Many2many("manual.attachment_experts",ondelete='restrict', string="Attachment", required=False)
    phone=fields.Char()

    
    
class remark(models.Model):
    _name = 'manual.remark'

    name = fields.Text()

class task(models.Model):
    _name = 'manual.task'
    
    book_id = fields.Many2one('manual.book', ondelete='restrict', string='BookID',required=False)
    experts_ids=fields.Many2many('manual.experts',ondelete='restrict', string='ExpertsID',required=False)
    remark_ids=fields.Many2many('manual.remark', ondelete='restrict', string='RemarkID',required=False)
    types_expertise_id=fields.Many2one('manual.types_expertise', ondelete='restrict', string="Type extertiseID", required=False)
    
class declarer(models.Model):
    _name = 'manual.declarer'

    name = fields.Char()
    lastName = fields.Char()
    
class mail(models.Model):
    _name = 'manual.mail'

    name=fields.Char(compute='_compute_name')
    number=fields.Char()
    declarer_id=fields.Many2one('manual.declarer', ondelete='restrict', string="DeclarerID", required=False)
    date_incoming_mail = fields.Date()
    order = fields.Char()
    executor_id = fields.Many2many('res.users', ondelete='restrict', string="ExecuterID", required=False)
    attachment=fields.Binary('Attacment',attachment=True)
    datas_fname = fields.Char("Filename",compute="_compute_datas_fname")
    reader_ids = fields.Many2many('res.users', ondelete='restrict', string="ReaderID", required=False)
    task_ids = fields.Many2many('manual.task', ondelete='restrict', string="BookID", required=False)
    department_id = fields.Many2one(comodel_name='hr.department',ondelete='set null')
    state = fields.Selection([
        ('confirmed', 'Confirmed'),
        ('planned', 'Planned'),
        ('progress', 'In Progress'),
        ('done', 'Done'),
        ('cancel', 'Cancelled')], string='State',
        copy=False, default='confirmed', track_visibility='onchange' ,compute='_state')
    outcoming_mail_id=fields.Many2many('manual.outcoming_mail', ondelete='restrict', string="Outcoming mail", required=True)
    date_outcoming_mail = fields.Date()
    number_outcoming_mail = fields.Char()
    types_expertise_id=fields.Many2one('manual.types_expertise', ondelete='restrict', string="Type extertiseID", required=False)
    attachmentOutcomingMail=fields.Binary(attachment=True)
    attachment_order=fields.Binary("Order attachment",attachment=True)
    order_date=fields.Date()
    datas_f = fields.Char("Filename",compute="_compute_datas_f")
    responsive_ids=fields.Many2many('res.users', ondelete='restrict', string="Responsive", required=False)

    @api.multi
    @api.depends('state')
    def _state(self):
        for x in self:
            if x.declarer_id:
                x.state='confirmed'
            if x.executor_id:
                x.state='planned'
            if x.task_ids:
                x.state='progress'
            
    @api.model
    def create(self, vals):
        Employee = self.env['hr.employee'].search([['user_id','=',vals['executor_id']]],limit=1)
        if(len(Employee)>0):
            vals['department_id']=Employee.department_id.id
        return super(mail, self).create(vals)

    # @api.depends('executor_id','reader_ids','task_ids')
    # def _status(self):
    #     for r in self:
    #         if len(r.executor_id)>0:
    #              r.status="1"
    #              if len(r.reader_ids)>0:
    #                 r.status="2"
    #                 if len(r.task_ids)>0:
    #                     r.status="3"

    @api.multi
    def _compute_datas_fname(self):
        self.datas_fname=str(self.date_incoming_mail)

    @api.multi
    def _compute_name(self):
        self.name=str(self.number)

    @api.multi
    def _compute_datas_f(self):
        self.datas_f=str(self.data_outcoming_mail)

# class outcoming_mail(models.Model):
#     _name = 'manual.outcoming_mail'

    
#     mail_id=fields.Many2many('manual.mail', ondelete='restrict', string="MailID", required=True)
    

class attachment_authors(models.Model):
    _name='manual.attachment_authors'

    attachment=fields.Binary('Attacment',attachment=True,)
    datas_fname = fields.Char("Filename",compute="_compute_datas_fname")
    doc_name=fields.Char()

    @api.multi
    def _compute_datas_fname(self):
        self.datas_fname="Hello"


class attachment_experts(models.Model):
    _name='manual.attachment_experts'

    attachment=fields.Binary('Attacment',attachment=True,)
    datas_fname = fields.Char("Filename",compute="_compute_datas_fname")
    doc_name=fields.Char()

    @api.multi
    def _compute_datas_fname(self):
        self.datas_fname=self.doc_name