from odoo import models, fields, api


class ResUsers(models.Model):
    _inherit = ['res.users']

    department_ids = fields.One2many('hr.department',compute="_compute_department")

    @api.one
    def _compute_department(self):
        deps = []
        Employee = self.employee_ids
        for emp in Employee:
            deps.append(emp.department_id.id)
            print(self.department_ids)
        self.department_ids = deps
