# -*- coding: utf-8 -*-
{
    'name': "Manual",

    'summary': """
        Expertising book""",

    'description': """
        Expertising book
    """,

    'author': "Dastan Ybrai",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Expertise',
    'version': '0.1',
    # any module necessary for this one to work correctly
    'depends': ['base','hr'], 

    # always loaded
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'views/class_number.xml',
        'views/science_degree.xml',
        'views/type_expertise.xml',
        'views/type_book.xml',
        'views/lang_book.xml',
        'views/experts.xml',
        'views/direction_book.xml',
        'views/type_organization.xml',
        'views/task.xml',
        #'views/outcoming_mail.xml',
        'views/mail.xml',
        'views/book.xml',
        'views/publishing_house.xml',
        'views/author.xml',
        'views/views.xml',
        'views/templates.xml',
    ],
    # only loaded in demonstration mode 
    'demo': [
        'demo/demo.xml',
    ],
    'application':True
}